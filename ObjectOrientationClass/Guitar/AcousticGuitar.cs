﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientationClass.Guitar
{
    public class AcousticGuitar : Guitar, IPlay
    {
        public string Play()
        {
            return $"{name} is playing some gentle chords";
        }
    }
}
