﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientationClass.Guitar
{
    public interface IPlay
    {
        string Play();
    }
}
