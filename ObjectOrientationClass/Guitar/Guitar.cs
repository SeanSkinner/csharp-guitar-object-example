﻿using ObjectOrientationClass.Guitar.GuitarStringsLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientationClass.Guitar
{
    public abstract class Guitar
    {

        protected string name;
        public string Name { get => name; set => name = value; }

        public GuitarStrings strings { get; set; } = new GuitarStrings();

        public Guitar()
        {

        }

        public Guitar(string name)
        {
            this.name = name;
        }

        public void CutString(int index)
        {
            strings[index] = 0;
            Console.WriteLine(strings.GetNumberOfRemainingStrings());
        }
    }
}
