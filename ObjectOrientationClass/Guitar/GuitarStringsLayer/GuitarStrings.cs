﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientationClass.Guitar.GuitarStringsLayer
{
    public class GuitarStrings
    {
        private int[] strings = new int[] { 1, 1, 1, 1, 1, 1 };

        public int this[int i]
        {
            get => strings[i];
            set => strings[i] = value;
        }

        public int GetNumberOfRemainingStrings()
        {
            return strings.Sum();
        }
    }
}
