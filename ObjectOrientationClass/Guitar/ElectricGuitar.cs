﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ObjectOrientationClass.Guitar
{
    public class ElectricGuitar:Guitar, IPlay
    {
        public string Pickups { get; set; }

        public BodyType Body { get; set; }

        public string Play()
        {
            return $"{name} is playing some power chords with the pickups {Pickups}";
        }
    }
}
