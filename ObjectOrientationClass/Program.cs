﻿using AmplifierLibrary;
using ObjectOrientationClass;
using ObjectOrientationClass.Guitar;

var electricGuitar = new ElectricGuitar { Name = "My Electric Guitar", Pickups = "EMG 81", Body = BodyType.SolidBody };
var acousticGuitar = new AcousticGuitar { Name = "My Acoustic Guitar" };

var guitars = new List<IPlay>();

guitars.Add(electricGuitar);
guitars.Add(acousticGuitar);

foreach (var guitar in guitars)
{
    Console.WriteLine(guitar.Play());
}

electricGuitar.CutString(0);

var point = new Point { x = 1, y = 2 };

Console.WriteLine($"x: {point.x}, y: {point.y}");

if (electricGuitar.Body == BodyType.HollowBody)
{
    Console.WriteLine("hit");
}